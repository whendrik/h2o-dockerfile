# Dockerfile to run h2o (`h20.jar`)

## Java, the JVM, and containers...

https://medium.com/adorsys/jvm-memory-settings-in-a-container-environment-64b0840e1d9e

See also *Make JVM respect CPU and RAM limits*...

https://hub.docker.com/_/openjdk

## h2o Documentation

http://docs.h2o.ai/h2o/latest-stable/h2o-docs/starting-h2o.html#from-the-command-line

## Example `build` command

build with for instance

```
docker build . -t h2o
```

## Build & Push the image in the IBM Cloud Registry

```
ibmcloud cr build --tag us.icr.io/willemh/openjdk_h2o:1 .
```

## Run in the code-engine

**21 October 2020:** The GUI Interface currently does not allow users enter a port to forward, and will forward `8080` by default. To use application with other ports, use the command line `ibmcloude ce` (or modify the default port of application).

```
ibmcloud ce app create --name h2o-instance --image us.icr.io/willemh/openjdk_h2o:1 --cpu 2 -m 2Gi -p 54321 --rs willemh
```