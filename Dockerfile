FROM openjdk:8

# Run Static commands

RUN \
# create user h2o
  useradd -ms /bin/bash h2o

# Add h2o, unfortunately, there is no .tar.gz image as we could directly extract it by piping it to tar

RUN \
  # Download to temporary file
  curl -o /tmp/h2o.zip https://h2o-release.s3.amazonaws.com/h2o/rel-zermelo/1/h2o-3.32.0.1.zip && \
  # Unzip to home directory
  unzip /tmp/h2o.zip -d /home/h2o && \
  # Remove temporary file
  rm /tmp/h2o.zip

# Add repository files

COPY --chown=h2o start.sh /home/h2o/start.sh
RUN chmod +x /home/h2o/start.sh

# Runtime behavior

WORKDIR /home/h2o
EXPOSE 54321
CMD ["./start.sh"]